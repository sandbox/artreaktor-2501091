INTRODUCTION
------------
Adds "Reorder" button field to the Line item view.
This project is similar to the Commerce Reorder, but allows to reorder an
individual line item, not the whole order. Reordered line items will be cloned
to the Cart.

REQUIREMENTS
------------
This module requires the following modules:

 * Commerce Cart (https://drupal.org/project/commerce_cart)
 * Commerce Order (https://drupal.org/project/commerce_order)

INSTALLATION
------------
Install and enable the module as usual.

CONFIGURATION
-------------
Add a "Reorder Line Item" button to your Line Item View.

Views Megarow
In order to support Views Megarow the Add-to-cart-redirect rule should be
disabled.

RULES
-----
The Rules "The line item is reordered" condition and
"After the line item has been reordered" event was provided.

MAINTAINERS
-----------
 * Dmitry Sukhovoy (artreaktor) - https://www.drupal.org/u/artreaktor
