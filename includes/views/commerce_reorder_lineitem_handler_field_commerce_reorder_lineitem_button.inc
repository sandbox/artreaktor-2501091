<?php

/**
 * @file
 * Reorder button views field handler.
 */

/**
 * Implements Reorder Line Item button field.
 */
class commerce_reorder_lineitem_handler_field_commerce_reorder_lineitem_button extends views_handler_field {

  /**
   * Default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['reorder_button_label'] = array('default' => 'Reorder', 'translatable' => TRUE);
    $options['redirect'] = array('default' => TRUE);
    $options['redirect_url'] = array('default' => 'cart');
    return $options;
  }

  /**
   * Field settings form.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['reorder_button_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Reorder button label.'),
      '#default_value' => $this->options['reorder_button_label'],
    );
  }

  /**
   * Constructor.
   */
  function construct() {
    parent::construct();
    $this->real_field = 'line_item_id';
    $this->additional_fields['line_item_id'] = 'line_item_id';
    $this->additional_fields['order_id'] = 'order_id';
    $this->additional_fields['data'] = 'data';
  }

  /**
   * Render form element placeholder.
   */
  function render($values) {
    $values->commerce_line_item_data = unserialize($values->commerce_line_item_data);
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

  /**
   * The reorder widget.
   */
  function views_form(&$form, &$form_state) {
    if (empty($this->view->result)) {
      return;
    }

    $form[$this->options['id']] = array(
      '#tree' => TRUE,
    );

    foreach ($this->view->result as $row_id => $row) {
      // Render button only on the product line items.
      if (isset($row->commerce_line_item_data['context']['product_ids'][0])) {
        if (is_numeric($row->commerce_line_item_data['context']['product_ids'][0])) {
          $line_item_id = $this->get_value($row, 'line_item_id');
          $order_id = $this->get_value($row, 'order_id');

          $form[$this->options['id']][$row_id] = array(
            '#type' => 'submit',
            '#value' => isset($this->options['reorder_button_label']) ? $this->options['reorder_button_label'] : t('Reorder'),
            '#name' => 'reorder-line-item-' . $row_id,
            '#line_item_id' => $line_item_id,
            '#order_id' => $order_id,
          );
        }
      }
    }
  }

  /**
   * Submit handler.
   */
  function views_form_submit($form, &$form_state) {

    $field_name = $this->options['id'];

    foreach (element_children($form[$field_name]) as $row_id) {

      if ($form_state['triggering_element']['#name'] == 'reorder-line-item-' . $row_id) {
        $line_item = commerce_line_item_load($form[$field_name][$row_id]['#line_item_id']);
        commerce_reorder_lineitem_helper($line_item);
      }
    }
  }

}
