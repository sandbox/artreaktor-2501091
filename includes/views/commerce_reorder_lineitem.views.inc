<?php

/**
 * @file
 * Defines views handlers.
 */

/**
 * Implements hook_views_data_alter().
 */
function commerce_reorder_lineitem_views_data_alter(&$data) {
  // Reorder button.
  $data['commerce_line_item']['commerce_reorder_lineitem_button'] = array(
    'field' => array(
      'title' => t('Reorder Line Item button.'),
      'help' => t('Display a button to copy the line item to a new order in cart status.'),
      'handler' => 'commerce_reorder_lineitem_handler_field_commerce_reorder_lineitem_button',
    ),
  );
}
