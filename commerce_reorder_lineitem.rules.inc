<?php
/**
 * @file
 * Rules integration.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_reorder_lineitem_rules_condition_info() {
  return array(
    'commerce_reorder_lineitem_condition_item_is_reordered' => array(
      'group' => 'Commerce Reorder Lineitem',
      'label' => t('The line item is reordered'),
      'parameter' => array(
        'commerce_line_item' => array(
          'type' => 'commerce_line_item',
          'label' => t('Reordered line item'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function commerce_reorder_lineitem_rules_event_info() {
  $events = array(
    'commerce_reorder_lineitem_item_reordered' => array(
      'label' => t('After the line item has been reordered'),
      'group' => t('Commerce Reorder Lineitem'),
      'variables' => array(
        'commerce_line_item' => array(
          'label' => t('Commerce Line Item'),
          'type' => 'commerce_line_item',
        ),
      ),
    ),
  );
  return $events;
}

/**
 * The "The line item is reordered" condition callback.
 */
function commerce_reorder_lineitem_condition_item_is_reordered($line_item) {
  if (isset($line_item->data['context']['reordered']) && $line_item->data['context']['reordered'] == TRUE) {
    return TRUE;
  }
  return FALSE;
}
